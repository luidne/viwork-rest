package br.edu.catolicato.si.viwork.modelo;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Luídne
 */
public class Postagem {
    
    public static final String TABELA = "";
    public static final String ID = "";
    public static final String TEXTO = "";
    public static final String DATA = "";
    
    private Integer id;
    private String texto;
    private Date data;

    public Postagem() {}

    public Postagem(Integer id) {
        this.id = id;
    }

    public Postagem(Integer id, String texto) {
        this.id = id;
        this.texto = texto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.texto);
        hash = 59 * hash + Objects.hashCode(this.data);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Postagem other = (Postagem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.texto, other.texto)) {
            return false;
        }
        return Objects.equals(this.data, other.data);
    }

}
